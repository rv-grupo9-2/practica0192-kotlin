package com.example.practica0192kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var btnSaludar: Button;
    private lateinit var txtNombre: EditText;
    private lateinit var lblSaludar: TextView;
    private lateinit var btnLimpiar: Button;
    private lateinit var btnCerrar: Button;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Relacionar los objetos
        btnSaludar = findViewById(R.id.btnSaludar)
        txtNombre = findViewById(R.id.txtNombre)
        lblSaludar = findViewById(R.id.lblSaludo)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnCerrar = findViewById(R.id.btnCerrar)

        //Codificar el boton para imprimir saludo
        btnSaludar.setOnClickListener {
            if(txtNombre.text.toString().isEmpty()) {
                Toast.makeText(this, "Faltó capturar información",
                    Toast.LENGTH_SHORT).show()
            } else {
                val str = "Hola ${txtNombre.text.toString()} ¿cómo estás?"
                lblSaludar.text = str
            }
        }

        //Codificar el boton para limpiar
        btnLimpiar.setOnClickListener {
            if(lblSaludar.text.toString() == ":: ::") {
                Toast.makeText(this, "No hay nada para limpiar",
                    Toast.LENGTH_SHORT).show()
            } else {
                lblSaludar.text = ":: ::"
            }
        }

        //Codificar el boton para cerrar la app
        btnCerrar.setOnClickListener {
            finish()
        }

    }
}